import os

import nimterop/[build, cimport]

static:
  # cDebug() # Print wrapper to stdout

  cAddStdDir()

  cSkipSymbol(@["OLM_MESSAGE_TYPE_PRE_KEY", "OLM_MESSAGE_TYPE_MESSAGE"])

proc olmPreBuild(outdir, header: string) =
  when isDefined(windows):
    rmFile(outdir & "\\Makefile")
  else:
    rmFile(outdir & "/Makefile")

const
  baseDir = getProjectCacheDir("nim-olm") # Download library within nimcache
  olmVersion {.strdefine.} = "3.2.7"

cPlugin:
  import strutils
  # Symbol renaming
  proc onSymbol*(sym: var Symbol) {.exportc, dynlib.} =
    # Get rid of leading and trailing underscores
    sym.name = sym.name.strip(chars = {'_'})

when isDefined(olmStd) and cSearchPath("olm/olm.h").fileExists:

  when defined(windows):
    const dynolm =
      when defined(cpu64):
        "olm64.dll"
      else:
        "olm32.dll"
  elif defined(posix):
    when defined(linux) or defined(FreeBSD):
      const dynolm = "libolm.so(.3|.1|.6|)"
    elif defined(osx):
      const dynolm = "libolm(.3|.1|).dylib"
    else:
      static:
        gecho "Unsupported OS!"
        doAssert(false)
  else:
    static:
      gecho "Unsupported OS!"
      doAssert(false)

  cImport(@[cSearchPath("olm/pk.h"), cSearchPath("olm/sas.h"), cSearchPath("olm/error.h"), cSearchPath("olm/olm.h")],
      dynlib = "dynolm", recurse = true)

else:
  getHeader(
    "olm/olm.h",
    giturl = "https://gitlab.matrix.org/matrix-org/olm",
    dlurl = "https://gitlab.matrix.org/matrix-org/olm/-/archive/" & olmVersion &
        "/olm-" & olmVersion & ".tar.gz",
    outdir = baseDir
  )

  # Wrap olmPath as returned from getHeader() and link statically
  # or dynamically depending on user input
  when not isDefined(olmStatic):
    const
      # get the parent directory of the olm.h header
      dir = parentDir(olmPath)
      # concatenate this with directory with the error.h header
      errorPath = joinPath(dir, "error.h")
      # concatenate this with directory with the sas.h header
      sasPath = joinPath(dir, "sas.h")
      # concatenate this with directory with the pk.h header
      pkPath = joinPath(dir, "pk.h")
      # concatenate this with directory with the pickle_encoding.h header
      picklePath = joinPath(dir, "pickle_encoding.h")
    cImport(@[picklePath, pkPath, sasPath, errorPath, olmPath], recurse = true,
        dynlib = "olmLPath") # Pass dynlib if not static link
  else:
    # static compilation is currently not supported, because it needs c++
    # inform user about this and abort here
    gecho "Static compilation is currently not supported"
    gecho "For further information about this, please refer to the REDME of nim-olm"
    doAssert(false)
    # cImport(olmPath, recurse = true)
