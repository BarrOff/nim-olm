import os, strutils

import nimterop/[build, cimport, globals]

static:
  # cDebug() # Print wrapper to stdout

  cAddStdDir()

  cSkipSymbol(@["OLM_MESSAGE_TYPE_PRE_KEY", "OLM_MESSAGE_TYPE_MESSAGE"])

proc olmPreBuild(outdir, header: string) =
  when isDefined(windows):
    rmFile(outdir & "\\Makefile")
  else:
    rmFile(outdir & "/Makefile")

const
  baseDir = getProjectCacheDir("nim-olm") # Download library within nimcache
  olmVersion {.strdefine.} = "3.2.1"

when isDefined(olmStd) and cSearchPath("olm/olm.h").fileExists:

  when defined(windows):
    const dynolm =
      when defined(cpu64):
        "olm64.dll"
      else:
        "olm32.dll"
  elif defined(posix):
    when defined(linux) or defined(FreeBSD):
      const dynolm = "libolm.so(.3|.1|.6|)"
    elif defined(osx):
      const dynolm = "libolm(.3|.1|).dylib"
    else:
      static:
        gecho "Unsupported OS!"
        doAssert(false)
  else:
    static:
      gecho "Unsupported OS!"
      doAssert(false)

  cImport(cSearchPath("olm/olm.h"), dynlib = "dynolm", recurse = true)

else:
  getHeader(
    "olm/olm.h",
    giturl = "https://gitlab.matrix.org/matrix-org/olm",
    dlurl = "https://gitlab.matrix.org/matrix-org/olm/-/archive/" & olmVersion &
        "/olm-" & olmVersion & ".tar.gz",
    outdir = baseDir
  )

  # Wrap olmPath as returned from getHeader() and link statically
  # or dynamically depending on user input
  when not isDefined(olmStatic):
    cImport(olmPath, recurse = true, dynlib = "olmLPath") # Pass dynlib if not static link
  else:
    # static compilation is currently not supported, because it needs c++
    # inform user about this and abort here
    gecho "Static compilation is currently not supported"
    gecho "For further information about this, please refer to the REDME of nim-olm"
    doAssert(false)
    # cImport(olmPath, recurse = true)
