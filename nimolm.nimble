# Package

version       = "0.1.2"
author        = "Joachim Kruth"
description   = "A wrapper for the libolm cryptographic library"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.2.0"
requires "nimterop >= 0.6.0"
