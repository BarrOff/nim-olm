# Notice

This repository has moved to [Codeberg](https://codeberg.org/BarrOff/nim-olm)

## Archive

Nim-olm is a [Nim](https://nim-lang.org) wrapper for the [libolm](https://gitlab.matrix.org/matrix-org/olm) library.

It is intentionally not published on Github, read [here](https://sneak.berlin/20200307/the-case-against-microsoft-and-github/) why.

Nim-olm is distributed as a [Nimble](https://github.com/nim-lang/nimble) package and depends on [nimterop](https://github.com/nimterop/nimterop) to generate the wrappers.


### Installing

Nim-olm can be installed via [Nimble](https://github.com/nim-lang/nimble):

```sh
nimble install https://gitea.com/BarrOff/nim-olm
```

This will download and install nim-olm in the standard Nimble package location, typically ~/.nimble. Once installed, it can be imported into any Nim program.

To use libolm installed in a standard system path like `/usr/` or `/usr/local`
the `-d:olmStd` flag has to be passed when compiling.

To download and build libolm from the official [repo](https://gitlab.matrix.org/matrix-org/olm), the `-d:olmDL` flag has to be passed when compiling.
The version of libolm to build can be set by `-d:olmVersion=<version>`, e.g.
`-d:olmVersion="3.2.1"`, the current default version to build is `3.1.2`.


### Usage

Nim-olm currently wraps libolm without any changes to it's C interface.
For usage of the API, please refer to it's official
[documentation](https://gitlab.matrix.org/matrix-org/olm/-/tree/master/docs).

The [maintainer](https://gitea.com/BarrOff) is **no expert in cryptography**,
and therefore won't write a higher-level wrapper. If you are familiar with
cryptography, pull requests providing such an implementation are highly welcome.

```nim
import nimolm

var
  utilityBuffer: seq[char] = newSeq[char](olm_utility_size())
  utility: ptr OlmUtility = olm_utility(addr(utilityBuffer[0]))
```

More examples can be found in the
[tests-directory](https://gitea.com/BarrOff/nim-olm/branch/master/tests).
These tests have been directly converted from the official [testsuite](https://gitlab.matrix.org/matrix-org/olm/-/tree/master/tests).


### Status

Nimterop currently only supports C, even though C++ support is in the works.
Using libolm statically is therefore currently not possible, because the static
library contains C and C++ code. The compiled binary has to be dynamically
linked.

When nimterop stabilizes C++ support, building statically will be added.


## Credits

Nim-olm wraps the libolm source code and all licensing terms of libolm apply to the usage of this package.

Without the following people nim-olm wouldn't be possible:
* [genotrance](https://github.com/genotrance) creator of nimterop and nimarchive


## Feedback

Nim-olm is a work in progress and any feedback or suggestions are welcome. It is hosted on Gitea with an MIT license so issues, forks and PRs are most appreciated.
