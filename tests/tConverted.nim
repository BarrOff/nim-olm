# The tests have been converted from here:
# https://gitlab.matrix.org/matrix-org/olm/-/tree/master/tests
# To run these tests, simply execute `nimble test`.

import unittest

import nimolm

test "Olm sha256 test":
  # This test corresponds to:
  # https://gitlab.matrix.org/matrix-org/olm/-/blob/master/tests/test_olm_sha256.cpp

  var
    utilityBuffer: seq[char] = newSeq[char](olm_utility_size())
    utility: ptr OlmUtility = olm_utility(addr(utilityBuffer[0]))
  check(43 == olm_sha256_length(utility))

  var
    output: array[43, char]
    inputString: string = "Hello, World"
    expectedOutput: string = "A2daxT/5zRU1zMffzfosRYxSGDcfQY3BNvLRmsH76KU"
  discard olm_sha256(utility, addr(inputString[0]), 12, addr(output[0]), 43)

  check(output[0..^1] == expectedOutput[0..^1])

test "Olm signature test":
  # This test corresponds to:
  # https://gitlab.matrix.org/matrix-org/olm/-/blob/master/tests/test_olm_signature.cpp

  type
    MockRandom = object
      tag: uint8
      current: uint8

  proc memset(dest: pointer, c: cint, n: csize_t) {.importc: "memset",
    header: "string.h".}

  proc memcpy(dest: pointer, src: pointer, n: csize_t) {.importc: "memcpy",
    header: "string.h".}

  proc createMockRandom(tag: uint8, offset: uint8 = 0'u8): MockRandom =
    result = MockRandom(tag: tag, current: offset)

  proc operator(mr: var MockRandom, buf: pointer, length: csize_t) =
    var
      bytes: ptr uint8 = cast[ptr uint8](buf)
      internLength: csize_t = length

    while internLength > 32:
      bytes[] = mr.tag
      memset(addr(cast[ptr UncheckedArray[uint8]](bytes)[1]), cint(mr.current), 31)
      internLength -= 32
      bytes = addr(cast[ptr UncheckedArray[uint8]](bytes)[32])
      inc(mr.current)
    if internLength != 0:
      bytes[] = mr.tag
      memset(addr(cast[ptr UncheckedArray[uint8]](bytes)[1]), cint(mr.current),
          csize_t(internLength - 1))

  proc checkMalloc(size: csize_t): ptr uint8 =
    if size == high(csize_t):
      check(size == high(csize_t))
    result = cast[ptr uint8](alloc(size))

  var
    mockRandomA: MockRandom = MockRandom(tag: uint8('A'), current: 0x00)
    accountBuffer: ptr uint8 = checkMalloc(olm_account_size())
    account: ptr OlmAccount = olm_account(accountBuffer)

    randomSize: csize_t = olm_create_account_random_length(account)
    random: ptr uint8 = checkMalloc(randomSize)

  mockRandomA.operator(random, randomSize)
  discard olm_create_account(account, random, randomSize)
  dealloc(random)

  const
    messageSize: csize_t = 12
  var
    messageString: string = "Hello, World"
  var
    message: ptr uint8 = checkMalloc(messageSize)
  memcpy(message, addr(messageString[0]), messageSize)

  let
    signatureSize: csize_t = olm_account_signature_length(account)
  var
    signature: ptr uint8 = checkMalloc(signatureSize)
  check(high(csize_t) != olm_account_sign(account, message, messageSize,
      signature, signatureSize))

  let
    idKeysSize: csize_t = olm_account_identity_keys_length(account)
  var
    idKeys: ptr uint8 = checkMalloc(idKeysSize)
  check(high(csize_t) != olm_account_identity_keys(account, idKeys, idKeysSize))

  discard olm_clear_account(account)
  dealloc(accountBuffer)

  var
    utilityBuffer: ptr uint8 = checkMalloc(olm_utility_size())
    utility: ptr OlmUtility = olm_utility(utility_buffer)

  check(high(csize_t) != olm_ed25519_verify(utility, addr(cast[
      ptr UncheckedArray[uint8]](idKeys)[71]), 43, message, messageSize,
      signature, signatureSize))

  discard olm_clear_utility(utility)
  dealloc(utilityBuffer)

  dealloc(idKeys)
  dealloc(signature)
  dealloc(message)
