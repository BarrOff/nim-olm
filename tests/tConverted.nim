# The tests have been converted from here:
# https://gitlab.matrix.org/matrix-org/olm/-/tree/master/tests
# To run these tests, simply execute `nimble test`.

import unittest

import nimolm

test "Olm sha256 test":
  # This test corresponds to:
  # https://gitlab.matrix.org/matrix-org/olm/-/blob/master/tests/test_olm_sha256.cpp

  var
    utilityBuffer: seq[char] = newSeq[char](olm_utility_size())
    utility: ptr OlmUtility = olm_utility(addr(utilityBuffer[0]))
  check(43 == olm_sha256_length(utility))

  var
    output: array[43, char]
    inputString: string = "Hello, World"
    expectedOutput: string = "A2daxT/5zRU1zMffzfosRYxSGDcfQY3BNvLRmsH76KU"
  discard olm_sha256(utility, addr(inputString[0]), 12, addr(output[0]), 43)

  check(output[0..^1] == expectedOutput[0..^1])

test "Olm signature test":
  # This test corresponds to:
  # https://gitlab.matrix.org/matrix-org/olm/-/blob/master/tests/test_olm_signature.cpp

  type
    MockRandom = object
      tag: uint8
      current: uint8

  proc memset(dest: pointer, c: cint, n: csize_t) {.importc: "memset",
    header: "string.h".}

  proc memcpy(dest: pointer, src: pointer, n: csize_t) {.importc: "memcpy",
    header: "string.h".}

  proc createMockRandom(tag: uint8, offset: uint8 = 0'u8): MockRandom =
    result = MockRandom(tag: tag, current: offset)

  proc operator(mr: var MockRandom, buf: pointer, length: csize_t) =
    var
      bytes: ptr uint8 = cast[ptr uint8](buf)
      internLength: csize_t = length

    while internLength > 32:
      bytes[] = mr.tag
      memset(addr(cast[ptr UncheckedArray[uint8]](bytes)[1]), cint(mr.current), 31)
      internLength -= 32
      bytes = addr(cast[ptr UncheckedArray[uint8]](bytes)[32])
      inc(mr.current)
    if internLength != 0:
      bytes[] = mr.tag
      memset(addr(cast[ptr UncheckedArray[uint8]](bytes)[1]), cint(mr.current),
          csize_t(internLength - 1))

  proc checkMalloc(size: csize_t): ptr uint8 =
    if size == high(csize_t):
      check(size == high(csize_t))
    result = cast[ptr uint8](alloc(size))

  var
    mockRandomA: MockRandom = MockRandom(tag: uint8('A'), current: 0x00)
    accountBuffer: ptr uint8 = checkMalloc(olm_account_size())
    account: ptr OlmAccount = olm_account(accountBuffer)

    randomSize: csize_t = olm_create_account_random_length(account)
    random: ptr uint8 = checkMalloc(randomSize)

  mockRandomA.operator(random, randomSize)
  discard olm_create_account(account, random, randomSize)
  dealloc(random)

  const
    messageSize: csize_t = 12
  var
    messageString: string = "Hello, World"
  var
    message: ptr uint8 = checkMalloc(messageSize)
  memcpy(message, addr(messageString[0]), messageSize)

  let
    signatureSize: csize_t = olm_account_signature_length(account)
  var
    signature: ptr uint8 = checkMalloc(signatureSize)
  check(high(csize_t) != olm_account_sign(account, message, messageSize,
      signature, signatureSize))

  let
    idKeysSize: csize_t = olm_account_identity_keys_length(account)
  var
    idKeys: ptr uint8 = checkMalloc(idKeysSize)
  check(high(csize_t) != olm_account_identity_keys(account, idKeys, idKeysSize))

  discard olm_clear_account(account)
  dealloc(accountBuffer)

  var
    utilityBuffer: ptr uint8 = checkMalloc(olm_utility_size())
    utility: ptr OlmUtility = olm_utility(utility_buffer)

  check(high(csize_t) != olm_ed25519_verify(utility, addr(cast[
      ptr UncheckedArray[uint8]](idKeys)[71]), 43, message, messageSize,
      signature, signatureSize))

  discard olm_clear_utility(utility)
  dealloc(utilityBuffer)

  dealloc(idKeys)
  dealloc(signature)
  dealloc(message)

suite "SAS tests":
  setup:
    var
      alicePrivate: array[32, uint8] = [
        0x77'u8, 0x07'u8, 0x6D'u8, 0x0A'u8, 0x73'u8, 0x18'u8, 0xA5'u8, 0x7D'u8,
        0x3C'u8, 0x16'u8, 0xC1'u8, 0x72'u8, 0x51'u8, 0xB2'u8, 0x66'u8, 0x45'u8,
        0xDF'u8, 0x4C'u8, 0x2F'u8, 0x87'u8, 0xEB'u8, 0xC0'u8, 0x99'u8, 0x2A'u8,
        0xB1'u8, 0x77'u8, 0xFB'u8, 0xA5'u8, 0x1D'u8, 0xB9'u8, 0x2C'u8, 0x2A'u8
      ]
      alicePublic: string = "hSDwCYkwp1R0i33ctD73Wg2/Og0mOBr066SpjqqbTmo"

      bobPrivate: array[32, uint8] = [
        0x5D'u8, 0xAB'u8, 0x08'u8, 0x7E'u8, 0x62'u8, 0x4A'u8, 0x8A'u8, 0x4B'u8,
        0x79'u8, 0xE1'u8, 0x7F'u8, 0x8B'u8, 0x83'u8, 0x80'u8, 0x0E'u8, 0xE6'u8,
        0x6F'u8, 0x3B'u8, 0xB1'u8, 0x29'u8, 0x26'u8, 0x18'u8, 0xB6'u8, 0xFD'u8,
        0x1C'u8, 0x2F'u8, 0x8B'u8, 0x27'u8, 0xFF'u8, 0x88'u8, 0xE0'u8, 0xEB'u8
      ]
      bobPublic: string = "3p7bfXt9wbTTW2HC7OQ1Nz+DQ8hbeGdNrfx+FG+IK08"

      aliceSasBuffer: seq[char] = newSeq[char](olm_sas_size())

    check(aliceSasBuffer.len != 0)

    var
      aliceSas: ptr OlmSAS = olm_sas(addr(aliceSasBuffer[0]))

    check(aliceSas != nil)

    discard olm_create_sas(aliceSas, addr(alicePrivate[0]), csize_t(alicePrivate.len))

    var
      bobSasBuffer: seq[char] = newSeq[char](olm_sas_size())

    check(bobSasBuffer.len != 0)

    var
      bobSas: ptr OlmSAS = olm_sas(addr(bobSasBuffer[0]))

    check(bobSas != nil)

    discard olm_create_sas(bobSas, addr(bobPrivate[0]), csize_t(bobPrivate.len))

    var
      pubkey: seq[char] = newSeq[char](olm_sas_pubkey_length(aliceSas))

  test "SAS generate bytes":
    discard olm_sas_get_pubkey(aliceSas, addr(pubkey[0]), uint32(pubkey.len))

    for i in 0..<olm_sas_pubkey_length(aliceSas):
      check(alicePublic[i] == pubkey[i])

    discard olm_sas_set_their_key(bobSas, addr(pubkey[0]), olm_sas_pubkey_length(bob_sas))

    discard olm_sas_get_pubkey(bobSas, addr(pubkey[0]), csize_t(pubkey.len))

    for i in 0..<olm_sas_pubkey_length(bobSas):
      check(bobPublic[i] == pubkey[i])

    discard olm_sas_set_their_key(aliceSas, addr(pubkey[0]), olm_sas_pubkey_length(aliceSas))

    var
      aliceBytes: array[6, uint8]
      bobBytes: array[6, uint8]

    discard olm_sas_generate_bytes(aliceSas, cstring("SAS"), 3, addr(aliceBytes[0]), 6)
    discard olm_sas_generate_bytes(bobSas, cstring("SAS"), 3, addr(bobBytes[0]), 6)

    check(aliceBytes == bobBytes)

  test "SAS calculate MAC":
    discard olm_sas_get_pubkey(aliceSas, addr(pubkey[0]), uint32(pubkey.len))

    for i in 0..<olm_sas_pubkey_length(aliceSas):
      check(alicePublic[i] == pubkey[i])

    discard olm_sas_set_their_key(bobSas, addr(pubkey[0]), olm_sas_pubkey_length(bob_sas))

    discard olm_sas_get_pubkey(bobSas, addr(pubkey[0]), csize_t(pubkey.len))

    for i in 0..<olm_sas_pubkey_length(bobSas):
      check(bobPublic[i] == pubkey[i])

    discard olm_sas_set_their_key(aliceSas, addr(pubkey[0]), olm_sas_pubkey_length(aliceSas))

    var
      aliceMac: seq[uint8] = newSeq[uint8](olm_sas_mac_length(aliceSas))
      bobMac: seq[uint8] = newSeq[uint8](olm_sas_mac_length(bobSas))

    discard olm_sas_calculate_mac(aliceSas, cstring("Hello World!"), 12, cstring("MAC"), 3, addr(aliceMac[0]), olm_sas_mac_length(aliceSas))
    discard olm_sas_calculate_mac(bobSas, cstring("Hello World!"), 12,  cstring("MAC"), 3, addr(bobMac[0]), olm_sas_mac_length(bobSas))

    for i in 0..<olm_sas_mac_length(aliceSas):
      check(aliceMac[i] == bobMac[i])

suite "Encryption Test Cases":
  setup:
    var
      alicePrivate: array[32, uint8] = [
        0x77'u8, 0x07'u8, 0x6D'u8, 0x0A'u8, 0x73'u8, 0x18'u8, 0xA5'u8, 0x7D'u8,
        0x3C'u8, 0x16'u8, 0xC1'u8, 0x72'u8, 0x51'u8, 0xB2'u8, 0x66'u8, 0x45'u8,
        0xDF'u8, 0x4C'u8, 0x2F'u8, 0x87'u8, 0xEB'u8, 0xC0'u8, 0x99'u8, 0x2A'u8,
        0xB1'u8, 0x77'u8, 0xFB'u8, 0xA5'u8, 0x1D'u8, 0xB9'u8, 0x2C'u8, 0x2A'u8
      ]
      alicePublic: string = "hSDwCYkwp1R0i33ctD73Wg2/Og0mOBr066SpjqqbTmo"

      bobPrivate: array[32, uint8] = [
        0x5D'u8, 0xAB'u8, 0x08'u8, 0x7E'u8, 0x62'u8, 0x4A'u8, 0x8A'u8, 0x4B'u8,
        0x79'u8, 0xE1'u8, 0x7F'u8, 0x8B'u8, 0x83'u8, 0x80'u8, 0x0E'u8, 0xE6'u8,
        0x6F'u8, 0x3B'u8, 0xB1'u8, 0x29'u8, 0x26'u8, 0x18'u8, 0xB6'u8, 0xFD'u8,
        0x1C'u8, 0x2F'u8, 0x8B'u8, 0x27'u8, 0xFF'u8, 0x88'u8, 0xE0'u8, 0xEB'u8
      ]
      bobPublic: string = "3p7bfXt9wbTTW2HC7OQ1Nz+DQ8hbeGdNrfx+FG+IK08"

      decryptionBuffer: seq[uint8] = newSeq[uint8](olm_pk_decryption_size())
      decryption: ptr OlmPkDecryption = olm_pk_decryption(addr(decryptionBuffer[0]))

      pubkey: seq[char] = newSeq[char](olm_pk_key_length())

    discard olm_pk_key_from_private(
      decryption,
      addr(pubkey[0]), uint32(pubkey.len),
      addr(alicePrivate[0]), uint32(sizeof(alicePrivate[0]) * alicePrivate.len)
    )

  test "Public Key Encryption/Decryption Test Case 1":

    for i in 0..<olm_pk_key_length():
      check(alicePublic[i] == pubkey[i])

    var
      alicePrivateBackOut: ptr UncheckedArray[uint8] = cast[ptr UncheckedArray[uint8]](alloc(olm_pk_private_key_length()))

    discard olm_pk_get_private_key(decryption, alicePrivateBackOut, olm_pk_private_key_length())

    for i in 0..<olm_pk_private_key_length():
      check(alicePrivate[i] == alicePrivateBackOut[i])

    dealloc(alicePrivateBackOut)

    var
      encryptionBuffer: seq[uint8] = newSeq[uint8](olm_pk_encryption_size())
      encryption: ptr OlmPkEncryption = olm_pk_encryption(addr(encryptionBuffer[0]))

    discard olm_pk_encryption_set_recipient_key(encryption, addr(pubkey[0]), uint32(pubkey.len))

    const
      plaintextLength: csize_t = 14
    var
      plaintext: cstring = "This is a test"

    let
      ciphertextLength: csize_t = olm_pk_ciphertext_length(encryption, plaintextLength)
    var
      ciphertextBuffer: ptr UncheckedArray[uint8] = cast[ptr UncheckedArray[uint8]](alloc(ciphertextLength))

      outputBuffer: seq[uint8] = newSeq[uint8](olm_pk_mac_length(encryption))
      ephemeralKey: seq[uint8] = newSeq[uint8](olm_pk_key_length())

    discard olm_pk_encrypt(
      encryption,
      addr(plaintext[0]), uint32(plaintextLength),
      addr(ciphertextBuffer[0]), uint32(ciphertextLength),
      addr(outputBuffer[0]), uint32(outputBuffer.len),
      addr(ephemeralKey[0]), uint32(ephemeralKey.len),
      addr(bobPrivate[0]), uint32(bobPrivate.len)
    )

    for i in 0..<olm_pk_key_length():
      check(bobPublic[i] == char(ephemeralKey[i]))

    let
      maxPlaintextLength: csize_t = olm_pk_max_plaintext_length(decryption, ciphertextLength)
    var
      plaintextBuffer: ptr UncheckedArray[uint8] = cast[ptr UncheckedArray[uint8]](alloc(maxPlaintextLength))

    discard olm_pk_decrypt(
      decryption,
      addr(ephemeralKey[0]), uint32(ephemeralKey.len),
      addr(outputBuffer[0]), uint32(outputBuffer.len),
      addr(ciphertextBuffer[0]), uint32(ciphertextLength),
      addr(plaintextBuffer[0]), uint32(maxPlaintextLength)
    )

    for i in 0..<plaintextLength:
      check(plaintext[i] == char(plaintextBuffer[i]))

    dealloc(ciphertextBuffer)
    dealloc(plaintextBuffer)

  test "Public Key Decryption pickling":
    var
      PICKLE_KEY: cstring = "secret_key"
      pickleBuffer: seq[uint8] = newSeq[uint8](olm_pickle_pk_decryption_length(decryption))
      expectedPickle: cstring = "qx37WTQrjZLz5tId/uBX9B3/okqAbV1ofl9UnHKno1eipByCpXleAAlAZoJgYnCDOQZDQWzo3luTSfkF9pU1mOILCbbouubs6TVeDyPfgGD9i86J8irHjA"

    discard olm_pickle_pk_decryption(
      decryption,
      addr(PICKLE_KEY[0]), uint8(PICKLE_KEY.len),
      addr(pickleBuffer[0]), uint8(pickleBuffer.len)
    )

    for i in 0..<olm_pickle_pk_decryption_length(decryption):
      check(expectedPickle[i] == char(pickleBuffer[i]))

    discard olm_clear_pk_decryption(decryption)

    for i in 0..<pubkey.len:
      pubkey[i] = char(0)

    discard olm_unpickle_pk_decryption(
      decryption,
      addr(PICKLE_KEY[0]), uint(PICKLE_KEY.len),
      addr(pickleBuffer[0]), uint(pickleBuffer.len),
      addr(pubkey[0]), uint(pubkey.len)
    )

    for i in 0..<olm_pk_key_length():
      check(alicePublic[i] == pubkey[i])

    # The following part does not compile because `olm_enc_output_length`
    # is not available
    #
    # const
    #   junkLength: csize_t = 1
    # var
    #   pickleLength: csize_t = olm_pickle_pk_decryption_length(decryption)
    #   junkPickle: seq[uint8] = newSeq[uint8](pickleLength + olm_enc_output_length(junkLength))
    #
    # discard olm_pickle_pk_decryption(
    #   decryption,
    #   addr(PICKLE_KEY[0]), uint(PICKLE_KEY.len),
    #   addr(junkPickle[0]), uint(pickleLength)
    # )
    #
    # let
    #   junkPickleLength: csize_t = add_junk_suffix_to_pickle(
    #     addr(PICKLE_KEY[0]), uint(PICKLE_KEY.len),
    #     addr(junkPickle[0]), uint(pickleLength),
    #     uint(junkLength)
    #   )
    #
    # check(csize_t(-1),
    #   olm_unpickle_pk_decryption(
    #     decryption,
    #     addr(PICKLE_KEY[0]), uint(PICKLE_KEY.len),
    #     addr(junkPickle[0]), uint(junkPickleLength),
    #     addr(pubkey[0]), uint(pubkey.len)
    #   )
    # )
    #
    # check(OLM_PICKLE_EXTRA_DATA, olm_pk_decryption_last_error_code(decryption))

    var
      ciphertext: string = "ntk49j/KozVFtSqJXhCejg"
      mac: cstring = "zpzU6BkZcNI"
      ephemeralKey: cstring = "3p7bfXt9wbTTW2HC7OQ1Nz+DQ8hbeGdNrfx+FG+IK08"

    let
      maxPlaintextLength: csize_t = olm_pk_max_plaintext_length(decryption, uint(ciphertext.len))
    var
      plaintextBuffer: seq[uint8] = newSeq[uint8](maxPlaintextLength)

    discard olm_pk_decrypt(
      decryption,
      addr(ephemeralKey[0]), uint(ephemeralKey.len),
      addr(mac[0]), uint(mac.len),
      addr(ciphertext[0]), uint(ciphertext.len),
      addr(plaintextBuffer[0]), uint(maxPlaintextLength)
    )

    var
      plaintext: cstring = "This is a test"

    for i in 0..<plaintext.len:
      check(plaintext[i] == char(plaintextBuffer[i]))

  test "Public Key Signing":

    var
      signingBuffer: seq[uint8] = newSeq[uint8](olm_pk_signing_size())
      signing: ptr OlmPkSigning = olm_pk_signing(addr(signingBuffer[0]))

    pubkey = newSeq[char](olm_pk_signing_public_key_length() + 1)

    discard olm_pk_signing_key_from_seed(
      signing,
      addr(pubkey[0]), uint(pubkey.len - 1),
      addr(alicePrivate[0]), uint(alicePrivate.len)
    )

    var
      message: string = "We hold these truths to be self-evident, that all men are created equal, that they are endowed by their Creator with certain unalienable Rights, that among these are Life, Liberty and the pursuit of Happiness."

      sigBuffer: seq[uint8] = newSeq[uint8](olm_pk_signature_length() + 1)

    discard olm_pk_sign(
      signing,
      cast[ptr uint8](addr(message[0])), uint(message.len),
      addr(sigBuffer[0]), olm_pk_signature_length()
    )

    var
      utilityBuffer: pointer = alloc(olm_utility_size())
      utility: ptr OlmUtility = cast[ptr OlmUtility](olm_utility(utilityBuffer))

      res: csize_t

    res = olm_ed25519_verify(
      utility,
      addr(pubkey[0]), olm_pk_signing_public_key_length(),
      cast[ptr uint8](addr(message[0])), uint(message.len),
      addr(sigBuffer[0]), uint(sigBuffer.len)
    )

    check(res == 0)

    sigBuffer[5] = uint8('m')

    res = olm_ed25519_verify(
      utility,
      addr(pubkey[0]), olm_pk_signing_public_key_length(),
      cast[ptr uint8](addr(message[0])), uint(message.len),
      addr(sigBuffer[0]), uint(sigBuffer.len)
    )

    check(res == cast[csize_t](-1))

    discard olm_clear_utility(utility)
    dealloc(utilityBuffer)

    discard olm_clear_pk_signing(signing)
